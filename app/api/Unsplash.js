import Unsplash, { toJson } from 'unsplash-js/native';

const DEFAULT_PER_PAGE = 20;

const unsplash = new Unsplash({
  applicationId: 'b98a677e9f515b2cad1d2f73e8213530dde7f6f675df40b1b131376e6d0b6408',
  secret: '07f13dc73bbd557f3246d39e418d1530a88713388f9b1f402be05a9e3c9537e0',
});

export const listPhotos = (page, perPage = DEFAULT_PER_PAGE) => unsplash.photos.listPhotos(page, perPage).then(toJson);
export const searchPhotos = (keyword, page, perPage = DEFAULT_PER_PAGE) => unsplash.search.photos(keyword, page, perPage).then(toJson);
