import * as actionTypes from './actions';

const initialState = {
  fetching: false,
  searching: false,
  nextPage: 1,
  photos: {},
  ids: [],
};

const photosIds = (state = initialState.ids, action) => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.PHOTOS_FETCHED: {
      const { photos } = payload;
      if (state.length === 0) {
        return photos.map(photo => photo.id);
      }
      return [...state, ...photos.map(photo => photo.id)];
    }
    default:
      return state;
  }
};

const createPhotos = photos => photos.reduce(
  (photosObj, {
    id, urls, width, height, user, color,
  }) => ({
    ...photosObj,
    [id]: {
      url: {
        full: urls.full,
        regular: urls.regular,
        thumb: urls.thumb,
      },
      dimensions: {
        width,
        height,
      },
      author: user.name,
      color,
    },
  }),
  {},
);

const photosById = (state = initialState.photos, action) => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.PHOTOS_FETCHED: {
      const { photos } = payload;
      if (Object.keys(state).length === 0) {
        return createPhotos(photos);
      }
      return {
        ...state,
        ...createPhotos(photos),
      };
    }
    default:
      return state;
  }
};

export default (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case actionTypes.PHOTOS_REQUESTED:
      return {
        ...initialState,
        fetching: true,
      };
    case actionTypes.MORE_PHOTOS_REQUESTED:
      return {
        ...state,
        fetching: true,
      };
    case actionTypes.PHOTOS_FETCHED: {
      return {
        ...state,
        fetching: false,
        nextPage: state.nextPage + 1,
        photos: photosById(state.photos, action),
        ids: photosIds(state.ids, action),
      };
    }
    default:
      return state;
  }
};
