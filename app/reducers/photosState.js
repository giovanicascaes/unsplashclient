import * as actionTypes from './actions';

export default (state = {}, action) => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.PHOTO_LOAD_INITIATED: {
      const { id } = payload;
      return {
        ...state,
        [id]: {
          thumbnailLoaded: false,
          imageLoaded: false,
        },
      };
    }
    case actionTypes.PHOTO_THUMB_LOADED: {
      const { id } = payload;
      return {
        ...state,
        [id]: {
          ...state[id],
          thumbnailLoaded: true,
        },
      };
    }
    case actionTypes.PHOTO_IMAGE_LOADED: {
      const { id } = payload;
      return {
        ...state,
        [id]: {
          ...state[id],
          imageLoaded: true,
        },
      };
    }
    case actionTypes.FETCH_FINAL_IMAGE: {
      const { id } = payload;
      return {
        ...state,
        [id]: {
          ...state[id],
          imageLoaded: false,
        },
      };
    }
    case actionTypes.PHOTOS_FETCHED: {
      if (payload.page > 1) {
        return state;
      }
      return {};
    }
    case actionTypes.CLEAR_PHOTOS_STATE:
      return {};
    default:
      return state;
  }
};
