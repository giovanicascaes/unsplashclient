export const PHOTOS_REQUESTED = 'PHOTOS_REQUESTED';
export const MORE_PHOTOS_REQUESTED = 'MORE_PHOTOS_REQUESTED';
export const PHOTOS_FETCHED = 'PHOTOS_FETCHED';
export const SEARCH_PHOTOS = 'SEARCH_PHOTOS';
export const CLEAR_PHOTOS = 'CLEAR_PHOTOS';

export const PHOTO_LOAD_INITIATED = 'PHOTO_LOAD_INITIATED';
export const PHOTO_THUMB_LOADED = 'PHOTO_THUMB_LOADED';
export const PHOTO_IMAGE_LOADED = 'PHOTO_IMAGE_LOADED';
export const CLEAR_PHOTOS_STATE = 'CLEAR_PHOTOS_STATE';

const createAction = (type, payload) => ({
  type,
  ...(payload && { payload }),
});

export default createAction;

export const fetchPhotos = () => createAction(PHOTOS_REQUESTED);
export const fetchMorePhotos = () => createAction(MORE_PHOTOS_REQUESTED);
export const searchPhotos = keyword => createAction(SEARCH_PHOTOS, { keyword });
export const initLoadPhoto = id => createAction(PHOTO_LOAD_INITIATED, { id });
export const photoThumbnailLoaded = id => createAction(PHOTO_THUMB_LOADED, { id });
export const photoImageLoaded = id => createAction(PHOTO_IMAGE_LOADED, { id });
