import { combineReducers } from 'redux';

import photos from './photos';
import photosState from './photosState';

export default combineReducers({ photos, photosState });
