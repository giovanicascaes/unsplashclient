import {
  fork, takeEvery, call, put, select,
} from 'redux-saga/effects';

import createAction, * as actions from '../actions';
import { listPhotos, searchPhotos } from '../../api/Unsplash';
import { page } from './selectors';

function* fetchPhotos() {
  const nextPage = yield select(page);
  const photos = yield call(listPhotos, nextPage);
  yield put(createAction(actions.PHOTOS_FETCHED, { photos }));
}

function* watchPhotoRequests() {
  yield takeEvery(actions.PHOTOS_REQUESTED, fetchPhotos);
}

export default function* rootSaga() {
  yield fork(watchPhotoRequests);
}
