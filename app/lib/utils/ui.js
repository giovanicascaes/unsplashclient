import { Platform } from 'react-native';

import { STATUS_BAR_HEIGHT, HEX_TO_RGB_REGEX } from '../constants/ui';

export const getIconForPlatform = (icon, focused = true) => {
  if (Platform.OS === 'android') {
    return `md-${icon}`;
  }
  const iosVariant = `ios-${icon}`;
  if (focused) {
    return iosVariant;
  }
  return `${iosVariant}-outline`;
};

const fluidTransitionFadeTransition = appear => (transitionInfo) => {
  const { progress, start, end } = transitionInfo;
  const opacityAnimation = progress.interpolate({
    inputRange: [0, start, end, 1],
    outputRange: appear ? [0, 0, 1, 1] : [1, 1, 0, 0],
    extrapolate: 'clamp',
  });
  return {
    opacity: opacityAnimation,
  };
};

export const fluidTransitionFadeTransitionAppear = fluidTransitionFadeTransition(true);

export const fluidTransitionFadeTransitionDisappear = fluidTransitionFadeTransition(false);

const hexToRgb = (hex) => {
  const rawRgb = HEX_TO_RGB_REGEX.exec(hex);
  return {
    r: parseInt(rawRgb[1], 16),
    g: parseInt(rawRgb[2], 16),
    b: parseInt(rawRgb[3], 16),
  };
};

export const colorRgbIsLight = (r, g, b) => r * 0.299 + g * 0.587 + b * 0.114 <= 186;

export const colorHexIsLight = (hex) => {
  const { r, g, b } = hexToRgb(hex);
  return colorRgbIsLight(r, g, b);
};
