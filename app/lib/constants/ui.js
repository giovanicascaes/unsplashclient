export const STATUS_BAR_HEIGHT = 32; // iPhone X
export const HEX_TO_RGB_REGEX = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
