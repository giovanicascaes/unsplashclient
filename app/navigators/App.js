import { FluidNavigator } from 'react-navigation-fluid-transitions';

import Main from '../components/screens/Main';
import PhotoDetail from '../components/screens/PhotoDetail';
import FullScreenPhoto from '../components/screens/FullScreenPhoto';

export default FluidNavigator(
  {
    Main: {
      screen: Main,
    },
    PhotoDetail: {
      screen: PhotoDetail,
    },
    FullScreenPhoto: {
      screen: FullScreenPhoto,
    },
  },
  {
    initialRouteName: 'Main',
  },
);
