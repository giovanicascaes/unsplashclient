import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import getStoreConfiguration from './config/storeConfig';
import App from './navigators/App';
import LoadingScreen from './components/LoadingScreen';

const { store, persistor } = getStoreConfiguration();

export default () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      {bootstrapped => {
        if (bootstrapped) {
          return <App />;
        }
        return <LoadingScreen />;
      }}
    </PersistGate>
  </Provider>
);
