import { connect } from 'react-redux';

import ProgressiveImage from './ProgressiveImage';
import * as actionCreators from '../../reducers/actions';

const mapStateToProps = ({ photosState }, { id }) => {
  const { thumbnailLoaded, imageLoaded } = photosState[id] || {};
  return {
    thumbnailLoaded,
    imageLoaded,
  };
};

const mapDispatchToProps = dispatch => ({
  initLoadPhoto: id => dispatch(actionCreators.initLoadPhoto(id)),
  photoThumbnailLoaded: id => dispatch(actionCreators.photoThumbnailLoaded(id)),
  photoImageLoaded: id => dispatch(actionCreators.photoImageLoaded(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProgressiveImage);
