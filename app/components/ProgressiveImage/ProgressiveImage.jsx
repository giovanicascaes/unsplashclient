import React from 'react';
import { StyleSheet, Animated, findNodeHandle } from 'react-native';
import FastImage from 'react-native-fast-image';
import { BlurView } from 'react-native-blur';

const styles = StyleSheet.create({
  backgroundColor: {
    backgroundColor: '#e1e4e8',
    flex: 1,
  },
});

Animated.FastImage = Animated.createAnimatedComponent(FastImage);
Animated.BlurView = Animated.createAnimatedComponent(BlurView);

export default class extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      backgroundOpacity: new Animated.Value(0),
      thumbnailOpacity: new Animated.Value(0),
      imageOpacity: new Animated.Value(0),
      priority: this._getPriority(),
      thumbViewRef: null,
      thumbKey: 0,
      imageKey: 0,
    };
  }

  _thumbnailLoaded = id => {
    this.setState({ thumbViewRef: findNodeHandle(this.thumbRef) }, () => {
      const { thumbnailLoaded, photoThumbnailLoaded } = this.props;
      if (!thumbnailLoaded) {
        this._animateLoading(this.state.thumbnailOpacity, () => photoThumbnailLoaded(id));
      }
    });
  };

  _imageLoaded = id => {
    const { imageLoaded, photoImageLoaded } = this.props;
    if (!imageLoaded) {
      this._animateLoading(this.state.imageOpacity, () => photoImageLoaded(id));
    }
  };

  _animateLoading = (animatedValue, callback) => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 400,
      useNativeDriver: true,
    }).start(callback);
  };

  _getPriority = () => {
    const { priority } = this.props;
    if (priority) {
      switch (priority) {
        case 'high':
          return FastImage.priority.high;
        case 'normal':
          return FastImage.priority.normal;
        case 'low':
          return FastImage.priority.low;
        default:
          return undefined;
      }
    }
  };

  _onThumbnailError = () => this.setState({ thumbKey: this.state.thumbKey + 1 });

  _onImageError = () => this.setState({ imageKey: this.state.imageKey + 1 });

  componentDidMount() {
    const { thumbnailLoaded, initLoadPhoto, id } = this.props;
    if (thumbnailLoaded === undefined) {
      this._animateLoading(this.state.backgroundOpacity, () => initLoadPhoto(id));
    }
  }

  render() {
    const {
      backgroundOpacity,
      thumbnailOpacity,
      imageOpacity,
      priority,
      thumbViewRef,
      thumbKey,
      imageKey,
    } = this.state;
    const {
      id,
      finalSrc,
      thumbSrc,
      style,
      thumbnailLoaded,
      imageLoaded,
      ...otherProps
    } = this.props;
    return (
      <>
        {!thumbnailLoaded && (
          <Animated.View style={[style, styles.backgroundColor, { opacity: thumbnailLoaded === undefined ? backgroundOpacity : 1 }]} />
        )}
        {!imageLoaded && (
          <Animated.View
            style={[StyleSheet.absoluteFill, { opacity: thumbnailLoaded ? 1 : thumbnailOpacity }]}
          >
            <Animated.FastImage
              {...otherProps}
              key={thumbKey}
              source={thumbSrc}
              priority={priority}
              style={style}
              ref={image => {
                this.thumbRef = image;
              }}
              onLoad={() => this._thumbnailLoaded(id)}
              onError={this._onThumbnailError}
            />
            <Animated.BlurView
              {...otherProps}
              style={[StyleSheet.absoluteFill, style]}
              viewRef={thumbViewRef}
              blurType="light"
              blurAmount={20}
            />
          </Animated.View>
        )}
        {thumbnailLoaded && (
          <Animated.FastImage
            {...otherProps}
            key={imageKey}
            source={finalSrc}
            priority={priority}
            style={[StyleSheet.absoluteFill, style, { opacity: imageLoaded ? 1 : imageOpacity }]}
            onLoad={() => this._imageLoaded(id)}
            onError={this._onImageError}
          />
        )}
      </>
    );
  }
}
