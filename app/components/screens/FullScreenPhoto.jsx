import React from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  Animated,
  ActivityIndicator,
} from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PhotoView from 'react-native-photo-view';

import {
  getIconForPlatform,
  fluidTransitionFadeTransitionAppear,
  fluidTransitionFadeTransitionDisappear,
} from '../../lib/utils/ui';
import { STATUS_BAR_HEIGHT } from '../../lib/constants/ui';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
  photo: {
    width: screenWidth,
    height: screenHeight,
  },
  emptyPhoto: {
    width: 0,
    height: 0,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  exitFullScreenButtonContainer: {
    position: 'absolute',
    top: STATUS_BAR_HEIGHT + 20,
    left: 20,
  },
  exitFullScreenButton: {
    paddingHorizontal: 8,
    paddingTop: 5,
    paddingBottom: 4,
    backgroundColor: 'white',
    opacity: 0.6,
    borderRadius: 31,
  },
});

Animated.PhotoView = Animated.createAnimatedComponent(PhotoView);

const PhotoViewLoader = class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      photoViewOpacity: new Animated.Value(0),
    };
  }

  _onLoadPhotoViewer = () =>
    this.setState({ loading: false }, () =>
      Animated.timing(this.state.photoViewOpacity, {
        toValue: 1,
        duration: 400,
        useNativeDriver: true,
      }).start(),
    );

  componentDidMount() {
    this.setState({ loading: true });
  }

  render() {
    const { url, onTap } = this.props;
    const { loading, photoViewOpacity } = this.state;
    return (
      <>
        {loading && <ActivityIndicator style={StyleSheet.absoluteFill} />}
        <Animated.PhotoView
          source={{ uri: url.full }}
          minimumZoomScale={1}
          maximumZoomScale={3}
          androidScaleType="center"
          style={[styles.photo, { opacity: photoViewOpacity }]}
          onLoad={this._onLoadPhotoViewer}
          onTap={onTap}
        />
      </>
    );
  }
};

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      elementsHidden: false,
      statusBarHidden: false,
      hideableElementsOpacity: new Animated.Value(1),
    };
  }

  _getPhotoHeight = (width, height) => {
    return Math.round((height * screenWidth) / width);
  };

  _toggleElementsVisibility = () => {
    const { elementsHidden, hideableElementsOpacity, statusBarHidden } = this.state;
    this.setState({ statusBarHidden: !statusBarHidden });
    if (elementsHidden) {
      this.setState(
        { elementsHidden: false, statusBarHidden: false },
        Animated.timing(hideableElementsOpacity, {
          toValue: 1,
          duration: 400,
          useNativeDriver: true,
        }).start(),
      );
    } else {
      this.setState({ statusBarHidden: true });
      Animated.timing(hideableElementsOpacity, {
        toValue: 0,
        duration: 400,
        useNativeDriver: true,
      }).start(() => this.setState({ elementsHidden: true }));
    }
  };

  render() {
    const { navigation } = this.props;
    const { elementsHidden, statusBarHidden, hideableElementsOpacity } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar
          hidden={statusBarHidden}
          backgroundColor="black"
          barStyle="light-content"
          animated
        />
        <PhotoViewLoader
          url={navigation.state.params.photo.url}
          onTap={this._toggleElementsVisibility}
        />
        {!elementsHidden && (
          <Transition
            appear={fluidTransitionFadeTransitionAppear}
            disappear={fluidTransitionFadeTransitionDisappear}
          >
            <Animated.View
              style={[styles.exitFullScreenButtonContainer, { opacity: hideableElementsOpacity }]}
            >
              <TouchableOpacity
                style={styles.exitFullScreenButton}
                onPress={() => navigation.goBack()}
              >
                <Ionicons name={getIconForPlatform('contract')} size={18} />
              </TouchableOpacity>
            </Animated.View>
          </Transition>
        )}
      </View>
    );
  }
}
