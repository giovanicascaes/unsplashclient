import { connect } from 'react-redux';

import Main from './Main';
import * as actionCreators from '../../../reducers/actions';

const mapStateToProps = ({ photos: { fetching, photos, ids }, photosState }) => {
  const [coverId, ...photoListIds] = ids;
  return {
    fetching,
    cover: { ...photos[coverId], id: coverId },
    photoList: photoListIds.map(id => ({ ...photos[id], id })),
    allPhotosLoadInitiated: Object.keys(photosState).length === ids.length,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchPhotos: () => dispatch(actionCreators.fetchPhotos()),
  fetchMorePhotos: () => dispatch(actionCreators.fetchMorePhotos()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
