import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Animated,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions';

import Header, { HEADER_MAX_HEIGHT } from '../Header/Header';
import ProgressiveImage from '../../ProgressiveImage';
import LoadingScreen from '../../LoadingScreen';

const { width: screenWidth } = Dimensions.get('window');
const PHOTO_LIST_NUM_COLUMNS = 2;
const PHOTO_MARGIN = 10;
const photoWidth = (screenWidth - PHOTO_MARGIN * 3) / PHOTO_LIST_NUM_COLUMNS;
const PHOTO_HEIGHT = 250;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  photoListContainer: {
    margin: 5,
  },
  photoList: {
    flex: 1,
  },
  photoContainer: {
    margin: 5,
  },
  photo: {
    width: photoWidth,
    height: PHOTO_HEIGHT,
    borderRadius: 10,
    flexBasis: 0,
  },
});

const PhotoListEndComponent = () => <ActivityIndicator style={{ marginVertical: 20 }} />;

export default class extends React.PureComponent {
  constructor(props) {
    super(props);

    const photoListScroll = new Animated.Value(-HEADER_MAX_HEIGHT);

    this.state = {
      photoListScroll,
      refreshing: false,
      initiating: true,
    };
  }

  _onPressPhoto = photo =>
    this.props.navigation.navigate('PhotoDetail', {
      photo,
    });

  _renderPhoto = ({ item }) => (
    <View style={styles.photoContainer}>
      <TouchableOpacity onPress={() => this._onPressPhoto(item)}>
        <Transition shared={item.id}>
          <ProgressiveImage
            finalSrc={{ uri: item.url.regular }}
            thumbSrc={{ uri: item.url.thumb }}
            style={styles.photo}
            id={item.id}
          />
        </Transition>
      </TouchableOpacity>
    </View>
  );

  _onRefreshPhotoList = () => this.setState({ refreshing: true }, this.props.fetchPhotos);

  _onPhotoListEndReached = () => {
    const { fetching, fetchPhotos, page, allPhotosLoadInitiated } = this.props;
    if (allPhotosLoadInitiated && !fetching) {
      fetchPhotos(page + 1);
    }
  };

  _getRefFlatList = flatList => {
    this._flatListRef = flatList;
  };

  _scrollListToOffsetFromHeader = y => {
    if (this._photoListOffsetY < y) {
      this._flatListRef.getNode().scrollToOffset({ offset: y });
    }
  };

  _checkRefreshCompleted = prevProps => {
    if (this.state.refreshing && prevProps.fetching && !this.props.fetching) {
      this.setState({
        refreshing: false,
      });
    }
  };

  _checkInitCompleted = prevProps => {
    if (this.state.initiating && prevProps.fetching && !this.props.fetching) {
      this.setState({
        initiating: false,
      });
    }
  };

  componentDidMount() {
    this.props.fetchPhotos();
  }

  componentDidUpdate(prevProps) {
    this._checkInitCompleted(prevProps);
    this._checkRefreshCompleted(prevProps);
  }

  render() {
    if (this.state.initiating) {
      return <LoadingScreen />;
    }

    const { cover, photoList, searchPhotos } = this.props;
    const { photoListScroll, refreshing } = this.state;

    return (
      <Animated.View style={styles.container}>
        <Animated.View style={styles.photoList}>
          <Animated.FlatList
            data={photoList}
            keyExtractor={item => item.id}
            renderItem={this._renderPhoto}
            numColumns={2}
            getItemLayout={(_, index) => {
              const photoLength = PHOTO_HEIGHT + PHOTO_MARGIN;
              return { length: photoLength, offset: photoLength * index, index };
            }}
            contentContainerStyle={styles.photoListContainer}
            onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: photoListScroll } } }], {
              useNativeDriver: true,
              listener: event => {
                this._photoListOffsetY = event.nativeEvent.contentOffset.y;
              },
            })}
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={1}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={this._onRefreshPhotoList}
                progressViewOffset={HEADER_MAX_HEIGHT}
              />
            }
            onEndReached={this._onPhotoListEndReached}
            onEndReachedThreshold={0.1}
            ListFooterComponent={PhotoListEndComponent}
            contentInset={{ top: HEADER_MAX_HEIGHT }}
            contentOffset={{ y: -HEADER_MAX_HEIGHT }}
            ref={this._getRefFlatList}
          />
        </Animated.View>
        <Header
          coverImageRegular={cover.url.regular}
          coverImageThumb={cover.url.thumb}
          coverColor={cover.color}
          photoListScroll={this.state.photoListScroll}
          scrollListToOffset={this._scrollListToOffsetFromHeader}
          id={cover.id}
          onSubmitSearch={searchPhotos}
        />
      </Animated.View>
    );
  }
}
