import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FastImage from 'react-native-fast-image';

import { STATUS_BAR_HEIGHT } from '../../lib/constants/ui';
import ProgressiveImage from '../ProgressiveImage';
import {
  getIconForPlatform,
  fluidTransitionFadeTransitionAppear,
  fluidTransitionFadeTransitionDisappear,
  colorHexIsLight,
} from '../../lib/utils/ui';

const PHOTO_HEIGHT = 300;
const { width: screenWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  photo: {
    height: PHOTO_HEIGHT,
  },
  description: {
    flex: 1,
    padding: 20,
  },
  authorText: {
    fontSize: 15,
    color: '#666',
  },
  author: {
    fontWeight: '700',
  },
  closeButtonContainer: {
    position: 'absolute',
    top: STATUS_BAR_HEIGHT + 20,
    left: 20,
  },
  closeButton: {
    paddingHorizontal: 9,
    paddingVertical: 4,
    backgroundColor: 'white',
    opacity: 0.6,
    borderRadius: 15,
  },
  closeText: {
    fontWeight: '700',
  },
  fullScreenButtonContainer: {
    alignSelf: 'flex-end',
    paddingRight: 20,
    position: 'absolute',
    bottom: 20,
  },
  fullScreenButton: {
    paddingHorizontal: 8,
    paddingTop: 5,
    paddingBottom: 4,
    backgroundColor: 'white',
    opacity: 0.6,
    borderRadius: 31,
  },
  preloadingFullImage: {
    width: 30,
  },
});

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statusBarBarStyle: 'default',
      imageKey: 0,
    };
  }
  _descriptionTransition = appear => transitionInfo => {
    const {
      progress,
      start,
      end,
      metrics: { height },
    } = transitionInfo;
    const opacityAnimation = progress.interpolate({
      inputRange: [0, start, end, 1],
      outputRange: appear ? [0, 0, 1, 1] : [1, 1, 0, 0],
      extrapolate: 'clamp',
    });
    const translateYAnimation = progress.interpolate({
      inputRange: [0, start, end, 1],
      outputRange: appear ? [height, height, 1, 1] : [1, 1, height, height],
      extrapolate: 'clamp',
    });
    return {
      opacity: opacityAnimation,
      transform: [{ translateY: translateYAnimation }],
    };
  };

  _descriptionTransitionAppear = this._descriptionTransition(true);

  _descriptionTransitionDisappear = this._descriptionTransition(false);

  _onPressFullScreen = () => {
    const { navigation } = this.props;
    navigation.navigate('FullScreenPhoto', {
      photo: navigation.state.params.photo,
    });
  };

  _getStatusBarBarStyle = () => {
    this.setState({
      statusBarBarStyle: colorHexIsLight(this.props.navigation.state.params.photo.color)
        ? 'dark-content'
        : 'light-content',
    });
  };

  _onImageFetchError = () => this.setState({ imageKey: this.state.imageKey + 1 });

  componentDidMount() {
    this._getStatusBarBarStyle();
  }

  render() {
    const { navigation } = this.props;
    const {
      photo: { id, url, author },
    } = navigation.state.params;
    return (
      <View style={styles.container}>
        <StatusBar barStyle={this.state.statusBarBarStyle} animated />
        <View style={styles.imageContainer}>
          <Transition shared={id}>
            <ProgressiveImage
              finalSrc={{ uri: url.regular }}
              thumbSrc={{ uri: url.thumb }}
              style={styles.photo}
              priority="high"
              id={id}
              onError={this._onImageFetchError}
            />
          </Transition>
          <Transition
            appear={fluidTransitionFadeTransitionAppear}
            disappear={fluidTransitionFadeTransitionDisappear}
          >
            <View style={styles.closeButtonContainer}>
              <TouchableOpacity style={styles.closeButton} onPress={() => navigation.goBack()}>
                <Text style={styles.closeText}>Close</Text>
              </TouchableOpacity>
            </View>
          </Transition>
          <Transition
            appear={fluidTransitionFadeTransitionAppear}
            disappear={fluidTransitionFadeTransitionDisappear}
          >
            <View style={styles.fullScreenButtonContainer}>
              <TouchableOpacity style={styles.fullScreenButton} onPress={this._onPressFullScreen}>
                <Ionicons name={getIconForPlatform('expand')} size={18} />
              </TouchableOpacity>
            </View>
          </Transition>
        </View>
        <Transition
          appear={this._descriptionTransitionAppear}
          disappear={fluidTransitionFadeTransitionDisappear}
        >
          <View style={styles.description}>
            <Text style={styles.authorText}>
              by <Text style={styles.author}>{author}</Text>
            </Text>
          </View>
        </Transition>
      </View>
    );
  }
}
