import React from 'react';
import { Animated, TextInput, StyleSheet, StatusBar } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { STATUS_BAR_HEIGHT } from '../../../lib/constants/ui';
import { getIconForPlatform, colorHexIsLight } from '../../../lib/utils/ui';
import ProgressiveImage from '../../ProgressiveImage';

export const INPUT_SEARCH_HEIGHT = 56;
export const HEADER_MIN_HEIGHT = STATUS_BAR_HEIGHT + INPUT_SEARCH_HEIGHT;
export const HEADER_MAX_HEIGHT = 300;
export const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const HEADER_COLOR_OPACITY_THRESHOLD = HEADER_SCROLL_DISTANCE * 0.7;

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
  },
  search: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    height: INPUT_SEARCH_HEIGHT,
    paddingTop: STATUS_BAR_HEIGHT,
    paddingLeft: 30,
    paddingRight: 30,
  },
  searchIcon: {
    paddingRight: 5,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: HEADER_MAX_HEIGHT,
  },
  backgroundColor: {
    height: HEADER_MAX_HEIGHT,
    backgroundColor: 'salmon',
  },
  searchInput: {
    flex: 1,
    marginBottom: 3,
  },
});

export default class extends React.Component {
  constructor(props) {
    super(props);

    const photoListScroll = Animated.add(props.photoListScroll, HEADER_MAX_HEIGHT);
    props.photoListScroll.addListener(this._trackPhotoListScroll);

    this.state = {
      headerMove: photoListScroll.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [0, -HEADER_SCROLL_DISTANCE],
        extrapolate: 'clamp',
      }),
      searchMove: photoListScroll.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [0, (HEADER_MAX_HEIGHT - STATUS_BAR_HEIGHT) / 2 - INPUT_SEARCH_HEIGHT / 2],
        extrapolate: 'clamp',
      }),
      headerColorOpacity: photoListScroll.interpolate({
        inputRange: [0, HEADER_COLOR_OPACITY_THRESHOLD, HEADER_SCROLL_DISTANCE],
        outputRange: [0, 0, 1],
        extrapolate: 'clamp',
      }),
      imageMove: photoListScroll.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [0, 100],
        extrapolate: 'clamp',
      }),
      contentColorStyle: 'default',
    };
  }

  _getContentColor = () => (this.state.contentColorStyle === 'dark-content' ? 'black' : 'white');

  _trackPhotoListScroll = ({ value }) => {
    if (value + HEADER_MAX_HEIGHT >= HEADER_COLOR_OPACITY_THRESHOLD * 1.3) {
      this.setState({ contentColorStyle: 'dark-content' });
    } else {
      this.setState({ contentColorStyle: 'light-content' });
    }
  };

  _defineContentColorStyle = () => {
    this.setState({
      contentColorStyle: colorHexIsLight(this.props.coverColor) ? 'dark-content' : 'light-content',
    });
  };

  _onFocusSearch = () => {
    
    this.props.scrollListToOffset(-HEADER_MIN_HEIGHT);
  };

  componentDidMount() {
    this._defineContentColorStyle();
  }

  render() {
    const { headerMove, headerColorOpacity, imageMove, searchMove, contentColorStyle } = this.state;
    const { id, coverImageRegular, coverImageThumb, searchPhotos } = this.props;

    return (
      <Animated.View style={[styles.header, { transform: [{ translateY: headerMove }] }]}>
        <StatusBar barStyle={contentColorStyle} />
        <ProgressiveImage
          finalSrc={{ uri: coverImageRegular }}
          thumbSrc={{ uri: coverImageThumb }}
          style={[
            styles.backgroundImage,
            {
              transform: [{ translateY: imageMove }],
            },
          ]}
          priority="high"
          id={id}
        />
        <Animated.View
          style={[StyleSheet.absoluteFill, styles.backgroundColor, { opacity: headerColorOpacity }]}
        />
        <Animated.View style={[styles.search, { transform: [{ translateY: searchMove }] }]}>
          <Ionicons
            name={getIconForPlatform('search')}
            size={22}
            style={styles.searchIcon}
            color={this._getContentColor()}
          />
          <TextInput
            placeholder="Search..."
            placeholderTextColor={this._getContentColor()}
            returnKeyType="search"
            returnKeyLabel="Search"
            style={styles.searchInput}
            onFocus={this._onFocusSearch}
            onSubmitEditing={searchPhotos}
          />
        </Animated.View>
      </Animated.View>
    );
  }
}
