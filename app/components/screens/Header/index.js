import { connect } from 'react-redux';

import Header from './Header';
import * as actionCreators from '../../../reducers/actions';

const mapDispatchToProps = dispatch => ({
  searchPhotos: keyword => dispatch(actionCreators.searchPhotos(keyword)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Header);
