import { applyMiddleware, createStore } from 'redux';
import { persistReducer, persistStore, createMigrate } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from 'redux-persist/lib/storage';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers';
import rootSaga from '../reducers/sagas';

const migrations = {
  22: state => ({
    photos: {
      fetching: false,
      searching: false,
      nextPage: 1,
      photos: {},
      ids: [],
    },
    photosState: {},
  }),
};

const rootPersistConfig = {
  key: 'root',
  version: 22,
  migrate: createMigrate(migrations),
  storage,
  stateReconciler: autoMergeLevel2,
};

const rootReducerPersisted = persistReducer(rootPersistConfig, rootReducer);
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducerPersisted,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
const persistor = persistStore(store);

export default () => {
  sagaMiddleware.run(rootSaga);
  return { store, persistor };
};
